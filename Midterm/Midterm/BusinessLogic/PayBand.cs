﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm.BusinessLogic
{
    class PayBand
    {
        public int MinimumPay { get; set; }
        public int MaximumPay { get; set; }
        public int YearlyStep { get; set; }

        public int PayRange
        {
            get
            {
                int Range = MaximumPay - MinimumPay;

                return Range;
            }
        }

        public PayBand(int minimumPay, int maximumPay, int yearlyPay)
        {
            MinimumPay = minimumPay;
            MaximumPay = maximumPay;
            YearlyStep = yearlyPay;
            

        }
    }
}
